public with sharing class TS4_CocktailController {
    
    @AuraEnabled(cacheable=true)
    public static List<Map<String,Object>> getCocktailsApi(String searchInput){
        String endpoint = 'https://www.thecocktaildb.com/api/json/v1/1/search.php?s=' + searchInput;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setMethod('GET');
        HttpResponse response = http.send(request);

        if(response.getStatusCode() == 200){
            String responseBody = response.getbody();
            
            // deserealizar la data del body
            Map<String, Object> respondeData = (Map<String, Object>) JSON.deserializeUntyped(responseBody);

            //validar si la llave drinks  tiene valor
            if(respondeData.containsKey('drinks') && respondeData.get('drinks') != null){
                List<Object> drinks = (List<Object>) respondeData.get('drinks');
                List<Map<String,Object>> data = new List<Map<String,Object>>();
                for(Object drinkObj : drinks){
                    Map<String,Object> drinkMap = (Map<String,Object>) drinkObj;
                    data.add(drinkMap);
                }

                return data;

            }else{
                return new List<Map<String,Object>>();
            }

        }else{
            return new List<Map<String,Object>>();
        }
    }

}