@isTest
public  class ts4_cocktailsTest {
    @isTest
    static void testgetCocktailsApi() {
        String searchInput = 'Margarita';

        Test.setMock(HttpCalloutMock.class , new MockHttResponseGenerador());

        List<Map<String,Object>> cocktails = TS4_CocktailController.getCocktailsApi(searchInput);

        System.assertNotEquals(null, cocktails);

    }

    public class MockHttResponseGenerador implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest request){
            HttpResponse response = HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.serBody('{"idDrink":"11007","strDrink":"Margarita"}');
            response.setStatusCode(200);
            return response
        }
    }
}