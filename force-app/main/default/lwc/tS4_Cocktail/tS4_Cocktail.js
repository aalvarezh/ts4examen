import { LightningElement, track } from 'lwc';
import TS4_CocktailController from '@salesforce/apex/TS4_CocktailController.getCocktailsApi'

export default class TS4_Cocktail extends LightningElement {
    
    @track cocktails = [];
    @track searchInput = '';
    @track showModal = false;
    @track selectedCocktail = {};
    

    handleSearchInputChange(event){
        this.searchInput = event.target.value
    }

    handleSearchClick(event){
        TS4_CocktailController({ searchInput : this.searchInput }).then(result =>{
            console.log('data q llega de la apex ', result)
            this.cocktails = result;
        }).catch(error =>{
            console.error('Error', error)
        })
    }

    handleModalOpen(event){
        const cocktailId = event.target.dataset.id
        this.selectedCocktail = this.cocktails.find(cocktail => cocktail.idDrink === cocktailId)
        this.showModal = true;
    }

    closeCocktailModal(event){
        this.showModal = false;

    }

}